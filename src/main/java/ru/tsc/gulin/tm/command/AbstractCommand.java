package ru.tsc.gulin.tm.command;

import ru.tsc.gulin.tm.api.model.ICommand;
import ru.tsc.gulin.tm.api.service.IServiceLocator;

public abstract class AbstractCommand implements ICommand{

    public abstract String getName();

    public abstract String getDescription();

    public abstract String getArgument();

    public abstract void execute();

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

}
